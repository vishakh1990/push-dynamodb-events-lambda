package com.amazonaws.lambda.demo.producer;

import java.util.List;
import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import com.amazonaws.lambda.demo.constants.UserRegnLambdaConstants;

public class UserRegnKafkaMsgService implements KafkaMsgService{

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Properties initializeProps() {
		
		Properties properties = new Properties();
        properties.put(UserRegnLambdaConstants.KAFKA_BOOTSTRAP_SERVERS_PROP, System.getenv(
        		UserRegnLambdaConstants.KAFKA_HOST_ENV_VAR_NAME));
        properties.put(UserRegnLambdaConstants.KAFKA_KEY_SERIALIZER_PROP, 
        		"org.apache.kafka.common.serialization.StringSerializer");
        properties.put(UserRegnLambdaConstants.KAFKA_VALUE_SERIALIZER_PROP, 
        		"org.apache.kafka.common.serialization.StringSerializer");
		return properties;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void publishMessage(List<String> messages) {
        KafkaProducer kafkaProducer = new KafkaProducer(this.initializeProps());
        try{
            for(String message : messages){
                kafkaProducer.send(new ProducerRecord(System.getenv(
                		UserRegnLambdaConstants.KAFKA_TOPIC_ENV_VAR_NAME), message));
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            kafkaProducer.close();
        }
		
	}

}
