package com.amazonaws.lambda.demo.producer;

import java.util.List;
import java.util.Properties;

public interface KafkaMsgService {

	/**
	 * initialize the properties associated with your Kafka broker
	 * @return
	 */
	Properties initializeProps();
	
	/**
	 * Publish the message to the kafka topic using kafka producer
	 * @param message
	 */
	void publishMessage(List<String> message);
	
}
