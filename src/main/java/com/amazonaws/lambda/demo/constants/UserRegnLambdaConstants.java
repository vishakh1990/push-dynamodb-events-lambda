package com.amazonaws.lambda.demo.constants;

public class UserRegnLambdaConstants {

	public static final String KAFKA_BOOTSTRAP_SERVERS_PROP ="bootstrap.servers";
	public static final String KAFKA_KEY_SERIALIZER_PROP ="key.serializer";
	public static final String KAFKA_VALUE_SERIALIZER_PROP ="value.serializer";
	public static final String KAFKA_HOST_ENV_VAR_NAME ="KAFKA_HOST";
	public static final String KAFKA_TOPIC_ENV_VAR_NAME ="KAFKA_TOPIC";
	
}
