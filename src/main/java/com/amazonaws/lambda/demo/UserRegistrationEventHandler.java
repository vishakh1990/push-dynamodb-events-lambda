package com.amazonaws.lambda.demo;

import java.util.ArrayList;
import java.util.List;

import com.amazonaws.lambda.demo.producer.KafkaMsgService;
import com.amazonaws.lambda.demo.producer.UserRegnKafkaMsgService;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;

/**
 * AWS Lambda Handler for user registration events on dynamo db
 * @author vishakh
 *
 */
public class UserRegistrationEventHandler implements RequestHandler<DynamodbEvent, Integer> {

	KafkaMsgService kafkaMsgService = new UserRegnKafkaMsgService();
	
	/**
	 * handler method for extracting record from dynamo DB stream and publishing relevant event information
	 * to the Kafka Topic on EC2
	 */
    @Override
    public Integer handleRequest(DynamodbEvent event, Context context) {
        
    	context.getLogger().log("Received a new Dynamo DB event: " + event);
        List<String> messages = new ArrayList<>();
        for (DynamodbStreamRecord record : event.getRecords()) {
            context.getLogger().log(record.getEventID());
            context.getLogger().log( record.getDynamodb().getKeys().get("userId").getS());
            messages.add("New " +record.getEventName() + " event by user with ID " + 
            record.getDynamodb().getKeys().get("userId").getS());
        }
        context.getLogger().log("Publishing Message to Kafka .. ");
        kafkaMsgService.publishMessage(messages);
        return event.getRecords().size();
    }
}